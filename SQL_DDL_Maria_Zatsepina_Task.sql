-- Create tables
CREATE TABLE "climbs" (
    "climb_id" serial NOT NULL,
    "group_id" int NOT NULL,
    "route_id" int NOT NULL,
    "date_and_time_of_climb_start" timestamp NOT NULL CHECK ("date_and_time_of_climb_start" > '2000-01-01'),
    "date_and_time_of_climb_end" timestamp NOT NULL,
    PRIMARY KEY("climb_id")
);

CREATE TABLE "groups" (
	"group_id" serial NOT NULL,
	"group_name" varchar(255) NOT NULL,
	"group_level" int NOT NULL DEFAULT 1,
	"group_supervisor_id" int NOT NULL,
	PRIMARY KEY("group_id")
);

CREATE TABLE "group_members" (
	"group-member_id" serial NOT NULL,
	"group_id" int NOT NULL,
	"partisipant_id" int NOT NULL,
	PRIMARY KEY("group-member_id")
);

CREATE TABLE "partisipants" (
    "partisipant_id" serial NOT NULL,
    "name" varchar(255) NOT NULL,
    "surname" varchar(255) NOT NULL,
    "adress_id" int NOT NULL,
    "supervision_experience_id" int NOT NULL,
    PRIMARY KEY("partisipant_id")
);

CREATE TABLE "countries" (
	"country_id" serial NOT NULL,
	"country_name" varchar(255) NOT NULL,
	PRIMARY KEY("country_id")
);

CREATE TABLE "adresses" (
    "adress_id" serial NOT NULL,
    "country_id" int NOT NULL,
    "city" varchar(255) NOT NULL,
    "street" varchar(255) NOT NULL,
    "house_number" int NOT NULL CHECK ("house_number" >= 0),
    "flat_number" int NOT NULL CHECK ("flat_number" >= 0),
    PRIMARY KEY("adress_id")
);

CREATE TABLE "supervision_experiences" (
	"supervision_experience_id" serial NOT NULL,
	"experience_points" int NOT NULL DEFAULT 0,
	"salary" money NOT NULL DEFAULT 0,
	PRIMARY KEY("supervision_experience_id")
);

CREATE TABLE "countries_locations" (
	"countries_locations_id" serial NOT NULL,
	"location_id" int NOT NULL,
	"country_id" int NOT NULL,
	PRIMARY KEY("countries_locations_id")
);

CREATE TABLE "mountains" (
	"mountain_id" serial NOT NULL,
	"name" varchar(255) NOT NULL,
	"height" int NOT NULL,
	PRIMARY KEY("mountain_id")
);

CREATE TABLE "terrains" (
    "terrain_id" serial NOT NULL,
    "type_of_terrain" varchar(255) NOT NULL,
    "danger_level" int NOT NULL CHECK ("danger_level" BETWEEN 1 AND 5),
    PRIMARY KEY("terrain_id")
);

CREATE TABLE "locations" (
	"location_id" serial NOT NULL,
	"mountain_id" int NOT NULL,
	"location_name" varchar(255) NOT NULL,
	PRIMARY KEY("location_id")
);

CREATE TABLE "mountains-terrains" (
	"mountain-terrain_id" serial NOT NULL,
	"mountain_id" int NOT NULL,
	"terrain_id" int NOT NULL,
	PRIMARY KEY("mountain-terrain_id")
);

CREATE TABLE "routes" (
	"route_id" serial NOT NULL,
	"complexity" int NOT NULL DEFAULT 0,
	"lenth" int NOT NULL DEFAULT 0,
	"route_name" varchar(255) NOT NULL,
	PRIMARY KEY("route_id")
);

CREATE TABLE "routes_locations" (
	"routes_locations_id" serial NOT NULL,
	"location_id" int NOT NULL,
	"route_id" int NOT NULL,
	PRIMARY KEY("routes_locations_id")
);

--Create relationships between tables using primary and foreign keys
ALTER TABLE "group_members"
ADD FOREIGN KEY("group_id") REFERENCES "groups"("group_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "climbs"
ADD FOREIGN KEY("group_id") REFERENCES "groups"("group_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "groups"
ADD FOREIGN KEY("group_supervisor_id") REFERENCES "partisipants"("partisipant_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "group_members"
ADD FOREIGN KEY("partisipant_id") REFERENCES "partisipants"("partisipant_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "partisipants"
ADD FOREIGN KEY("adress_id") REFERENCES "adresses"("adress_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "partisipants"
ADD FOREIGN KEY("supervision_experience_id") REFERENCES "supervision_experiences"("supervision_experience_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "adresses"
ADD FOREIGN KEY("country_id") REFERENCES "countries"("country_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "countries_locations"
ADD FOREIGN KEY("country_id") REFERENCES "countries"("country_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "countries_locations"
ADD FOREIGN KEY("location_id") REFERENCES "locations"("location_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "locations"
ADD FOREIGN KEY("mountain_id") REFERENCES "mountains"("mountain_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mountains-terrains"
ADD FOREIGN KEY("mountain_id") REFERENCES "mountains"("mountain_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "mountains-terrains"
ADD FOREIGN KEY("terrain_id") REFERENCES "terrains"("terrain_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "routes_locations"
ADD FOREIGN KEY("location_id") REFERENCES "locations"("location_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "routes_locations"
ADD FOREIGN KEY("route_id") REFERENCES "routes"("route_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "climbs"
ADD FOREIGN KEY("route_id") REFERENCES "routes"("route_id")
ON UPDATE NO ACTION ON DELETE NO ACTION;

-- Insert sample data 
INSERT INTO countries (country_name) VALUES ('Austria'), ('China');
INSERT INTO mountains (name, height) VALUES ('Grossglockner', 3798), ('Everest', 8848);
INSERT INTO terrains (type_of_terrain, danger_level) VALUES ('Rocky', 3), ('Icy', 4);
INSERT INTO locations (mountain_id, location_name) VALUES (1, 'Grossglockner scout station'), (2, 'Everest hicking camp');
INSERT INTO supervision_experiences (experience_points, salary) VALUES (3, 5000), (5, 8000);
INSERT INTO adresses (country_id, city, street, house_number, flat_number) VALUES 
(1, 'Vienna', 'KeiserMuller', 10, 5),
(2, 'Pekin', 'LaoBao', 20, 10);
INSERT INTO partisipants (name, surname, adress_id, supervision_experience_id) VALUES 
('Tom', 'Riddle', 1, 1),
('JoJo', 'Queen', 2, 2);
INSERT INTO groups (group_name, group_level, group_supervisor_id) VALUES ('Arctic Monkeys', 3, 1), ('Avangers', 4, 2);
INSERT INTO routes (complexity, lenth, route_name) VALUES (2, 10, 'Austian Camping'), (3, 25, 'Extreme climb');
INSERT INTO routes_locations (location_id, route_id) VALUES (1, 1), (2, 2);
INSERT INTO climbs (group_id, route_id, date_and_time_of_climb_start, date_and_time_of_climb_end) 
VALUES (1, 1, '2024-05-01 08:00:00', '2024-05-01 15:00:00'), 
(2, 2, '2024-05-02 09:00:00', '2024-05-02 16:00:00');
INSERT INTO group_members (group_id, partisipant_id) VALUES (1, 1), (2, 2);

-- Add 'record_ts' field to each table
ALTER TABLE climbs ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE groups ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE group_members ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE partisipants ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE countries ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE adresses ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE supervision_experiences ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE countries_locations ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE mountains ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE terrains ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE locations ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE "mountains-terrains" ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE routes ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE routes_locations ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;

-- Check if the 'record_ts' field has been set for existing rows in each table
SELECT * FROM climbs;
SELECT * FROM groups;
SELECT * FROM group_members;
SELECT * FROM partisipants;
SELECT * FROM countries;
SELECT * FROM adresses;
SELECT * FROM supervision_experiences;
SELECT * FROM countries_locations;
SELECT * FROM mountains;
SELECT * FROM terrains;
SELECT * FROM locations;
SELECT * FROM "mountains-terrains";
SELECT * FROM routes;
SELECT * FROM routes_locations;
SELECT * FROM climbs;